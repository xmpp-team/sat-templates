��          �      l      �  5   �          -  *   B  	   m  0   w     �     �  ;   �  /        1     >  ,   O  0   |  V   �                         (  �  ,  -        <     I     ^     z  @   �     �     �  )   �  -        @     P  !   a  (   �  K   �     �                    (                    
                                                      	                        An error occured while trying to access the resource. Click to to expand… Error %(error_code)s How many people will come (including you)? Not Found Please indicate if you plan to attend the event: Powered by %(app_name)s Send Sorry, we can't find the resource you are trying to access. Sorry, you are not allowed to access this page. Unauthorized Welcome %(name)s You have been disconnected, have a nice day! You have been invited to participate to an event You have been invited to participate with the community, please choose an action below Your comment maybe no show comments yes Project-Id-Version: sat VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-05-20 19:37+0200
PO-Revision-Date: 2017-05-20 21:41+0200
Last-Translator: strriga <strriga@gmail.com>
Language-Team: Slovak <>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.4.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;X-Generator: Gtranslator 2.91.7
X-Generator: Gtranslator 2.91.7
 Počas prístupu ku zdroju sa vyskytla chyba. Rozbaliť… Chyba %(error_code)s Koľko ľudí vrátane Vás Nenájdené Prosím, dajte vedieť, či sa plánujete zúčastniť udalosti: Powered by %(app_name)s Odoslať Prepáčte, nie je možné nájsť zdroj. Prepáčte, k tejto stránke nmáte prístup. Neautorizované Vitajte %(name)s Bol ste odhlásený, pekný deň! Boli ste pozvaný k účasti na udalosti Bol ste pozvaný k účasti so spoločnostou, prosím vyberte si akciu dole Váš komentár možno nie zobraziť komentáre áno 